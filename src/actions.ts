import { activity } from './activity'

const inquirer = require('inquirer')

export namespace actions {

  const addAction = async function (activities: Array<activity.Activity>): Promise<void> {
    const answers = await inquirer.prompt([
      {
        type: 'input',
        name: 'title',
        message: 'title: '
      },
      {
        type: 'input',
        name: 'description',
        message: 'description: '
      },
      {
        type: 'list',
        name: 'recurrence',
        message: 'choose activity recurrence: ',
        choices: [
          // get a list of all activity.Recurrency anum values.
          ...(Object.keys(activity.Recurrency).filter(k => typeof activity.Recurrency[k as any] === 'string'))
        ]
      },
      {
        type: 'input',
        name: 'deadline',
        message: 'deadline in format yyyy/mm/dd [None]: '
      }
    ])
    console.log({ answers }) // debug
    const { title, description, recurrence, deadline } = answers
    activities.push(new activity.Activity(title, description, recurrence, deadline))
  }

  const delAction = async function (activities: Array<activity.Activity>): Promise<Array<activity.Activity>> {
    const answer = await inquirer.prompt([
      {
        type: 'list',
        name: 'title',
        message: 'choose activity by title: ',
        choices: [
          '*cancel operation*',
          ...(activities.map(activity => activity.title))
        ]
      }
    ])
    console.log({ answer }) // debug
    return activities.filter(activity => activity.title !== answer.title)
  }

  const modAction = async function (activities: Array<activity.Activity>): Promise<Array<activity.Activity>> {
    const answer = await inquirer.prompt([
      {
        type: 'list',
        name: 'title',
        message: 'choose activity by title: ',
        choices: [
          '*cancel operation*',
          ...(activities.map(activity => activity.title))
        ]
      }
    ])
    console.log({ answer }) // debug
    const activity = activities.find(activity => activity.title === answer.title)
    const answer1 = await inquirer.prompt([
      {
        type: 'list',
        name: 'field',
        message: 'choose what field to edit: ',
        choices: [
          '*cancel operation*',
          ...(activity ? Object.keys(activity).map(str => str.replace(/_/g, '')) : [])
        ]
      }
    ])
    console.log({ answer1 }) // debug
    const { field } = answer1
    const answer2 = await inquirer.prompt([
      {
        type: 'input',
        name: 'input',
        message: `${field}: `
      }
    ])
    console.log({ answer2 }) // debug
    if (activity) {
      const idx = activities.indexOf(activity)
      activities[idx][field] = answer2.input
      activities[idx].updatedAt = Date.now()
    }
    return activities
  }

  export const launchAction = async function (cmd: string, activities: Array<activity.Activity>): Promise<activity.Activity[]> {
    if (/^add$/i.test(cmd)) {
      console.log('add activity command') // debug
      await addAction(activities)
    } else if (/^del$/i.test(cmd)) {
      console.log('delete activity command') // debug
      activities = await delAction(activities)
    } else if (/^mod$/i.test(cmd)) {
      console.log('modify activity command') // debug
      activities = await modAction(activities)
    } else {
      console.log('command not recognised')
    }

    console.log({ activities }) // debug
    return activities
  }

}
