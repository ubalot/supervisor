import { actions } from './actions'
import { activity } from './activity'

const fs = require('fs')
const inquirer = require('inquirer')

const dbFile = 'activities_db.txt'

const greetings = `
SuperVisor activated

`

const message = `
----------------------------------------------------------------------------
This is your current list of activities
----------------------------------------------------------------------------
`

const readFromFile = function (): Array<activity.Activity> {
  try {
    if (!fs.existsSync(dbFile)) {
      fs.writeFileSync(dbFile, JSON.stringify([]), 'utf8')
    }
    const json = JSON.parse(fs.readFileSync(dbFile, 'utf8'))
    return json.map(a => new activity.Activity(a.title, a.description, a.recurrence, a.createdAd, a.updatedAt))
  } catch (err) {
    console.log(err)
    return []
  }
}

const writeToFile = function (activities: Array<activity.Activity>): void {
  try {
    // humar readable json for debuggin purpose, prefere the minified version
    const json = JSON.stringify(activities.map(activity => activity.toJson()), null, '\t')
    // const json = JSON.stringify(activities.map((activity) => activity.toJson()))  // minified json
    fs.writeFileSync(dbFile, json, 'utf8')
  } catch (err) {
    console.log(err)
  }
}

async function promptUser (activities: Array<activity.Activity>): Promise<Array<activity.Activity>> {
  const answer = await inquirer.prompt([{
    type: 'input',
    name: 'cmd',
    message: 'SV> '
  }])
  console.log({ answer }) // debug
  const { cmd } = answer
  if (/^(e(xit)?|q(uit)?)$/i.test(cmd)) {
    return activities
  }
  activities = await actions.launchAction(cmd, activities)
  return promptUser(activities)
}

const main = async () => {
  let activities = readFromFile()

  console.log(greetings)
  console.log(message)
  console.log('activites: ')
  activities.forEach(activity => {
    console.log(` *  ${activity}`)
  })
  // give user some space
  console.log(`

  `)

  activities = await promptUser(activities)

  writeToFile(activities)
}

main()
