export namespace activity {

  export enum Recurrency {
    Once = 'once',
    Daily = 'daily',
    Weekly = 'weekly',
    Monthly = 'monthly',
    Yearly = 'yearly'
  }

  export class Activity {
    private _title: string
    private _description: string // will be optional?
    private _recurrence: Recurrency
    private _deadline?: Date
    private _createdAt: number
    private _updatedAt: number

    public get title (): string { return this._title }
    public set title (value: string) { this._title = value }
    public get description (): string { return this._description }
    public set description (value: string) { this._description = value }
    public get recurrence (): Recurrency { return this._recurrence }
    public set recurrence (value: Recurrency) { this._recurrence = value }
    public get deadline (): Date | undefined { return this._deadline }
    public set deadline (value: Date | undefined) { this._deadline = value }
    public get createdAt (): number { return this._createdAt }
    public set createdAt (value: number) { this._createdAt = value }
    public get updatedAt (): number { return this._updatedAt }
    public set updatedAt (value: number) { this._updatedAt = value }

    constructor (title: string, description: string, recurrence: Recurrency, deadline = undefined, createdAt = null, updatedAt = null) {
      this._title = title
      this._description = description
      this._recurrence = recurrence
      this._deadline = deadline
      this._createdAt = createdAt || Date.now()
      this._updatedAt = updatedAt || this._createdAt
    }

    public toJson (): Object {
      return {
        title: this.title,
        description: this.description,
        recurrence: this.recurrence,
        deadline: this.deadline,
        createdAd: this.createdAt,
        updatedAt: this.updatedAt
      }
    }

    public toString (): string {
      return Object.keys(this)
        .map(str => str.replace(/_/g, ''))
        .map(attr => `${attr}: ${this[attr]}`)
        .join(', ') + '\n'
    }
  }

}
