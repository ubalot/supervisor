# SuperVisor

### Compile TypeScript
```bash
npx tsc
```

### Run cli
```bash
npm run sv-cli
```

### Compile and run
```bash
npx tsc && npm run sv-cli
```
